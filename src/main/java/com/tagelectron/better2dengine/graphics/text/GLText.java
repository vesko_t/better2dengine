package com.tagelectron.better2dengine.graphics.text;

import com.tagelectron.better2dengine.graphics.shaders.AbstractShader;
import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.joml.Vector3f;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.GL_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL20.*;

public class GLText {

    private GLTrueTypeFont glTrueTypeFont;

    private String text;

    private Matrix4f model = new Matrix4f().identity();

    private Vector2f position;

    private static AbstractShader shader;

    private int vertexId;

    private int textureId;

    private int vertexCount;

    private boolean generateVBOs;

    private float length;

    private float height;

    private Vector3f color = new Vector3f();

    private float opacity = 1;

    private float maxLength = -1;

    public GLText(GLTrueTypeFont glTrueTypeFont, String text) {
        this(glTrueTypeFont, text, -1);
    }

    public GLText(GLTrueTypeFont glTrueTypeFont, String text, float lineLength) {
        this.glTrueTypeFont = glTrueTypeFont;
        this.text = text;
        glTrueTypeFont.generateVbos(this, lineLength);
    }

    public GLText(GLTrueTypeFont GLTrueTypeFont, String text, Vector2f position) {
        this.glTrueTypeFont = GLTrueTypeFont;
        this.text = text;
        this.position = new Vector2f(position.x, position.y + GLTrueTypeFont.getFontSize());
    }

    public GLText(GLTrueTypeFont GLTrueTypeFont, Vector2f position) {
        this.glTrueTypeFont = GLTrueTypeFont;
        this.position = new Vector2f(position.x, position.y + GLTrueTypeFont.getFontSize());
    }

    public GLText(GLTrueTypeFont glTrueTypeFont, String text, Vector3f color) {
        this.glTrueTypeFont = glTrueTypeFont;
        this.text = text;
        this.color = color;
        generateVBOs = true;
    }

    public GLText(GLTrueTypeFont GLTrueTypeFont, String text, Vector2f position, Vector3f color) {
        this.glTrueTypeFont = GLTrueTypeFont;
        this.text = text;
        this.color = color;
        this.position = new Vector2f(position.x, position.y + GLTrueTypeFont.getFontSize());
        this.generateVBOs = true;
    }

    public GLText(GLTrueTypeFont GLTrueTypeFont, Vector2f position, Vector3f color) {
        this.glTrueTypeFont = GLTrueTypeFont;
        this.color = color;
        this.position = new Vector2f(position.x, position.y + GLTrueTypeFont.getFontSize());
    }

    public void render() {
        if (text != null && !text.equals("")) {
            model.setTranslation(new Vector3f(position, 0));
            shader.bind();
            shader.setUniform("model", model);
            shader.setUniform("projection", GuiManager.getProjection());
            shader.setUniform("sampler", 0);
            shader.setUniform("color", color);
            shader.setUniform("opacity", opacity);

            if (generateVBOs) {
                glTrueTypeFont.generateVbos(this, maxLength);
                generateVBOs = false;
            }

            glTrueTypeFont.bindAtlas();

            glEnableVertexAttribArray(0);
            glEnableVertexAttribArray(1);

            glBindBuffer(GL_ARRAY_BUFFER, textureId);
            glVertexAttribPointer(1, 2, GL_FLOAT, false, 0, 0);

            glBindBuffer(GL_ARRAY_BUFFER, vertexId);
            glVertexAttribPointer(0, 2, GL_FLOAT, false, 0, 0);

            glDrawArrays(GL_QUADS, 0, vertexCount);

            glDisableVertexAttribArray(0);
            glDisableVertexAttribArray(1);
        }
    }

    public void setText(String text) {
        this.text = text;
        generateVBOs = true;
    }

    public Vector2f getPosition() {
        return position;
    }

    public void setPosition(Vector2f position) {
        this.position = new Vector2f(position.x, position.y + glTrueTypeFont.getFontSize());
    }

    void setVertexId(int vertexId) {
        this.vertexId = vertexId;
    }

    void setTextureId(int textureId) {
        this.textureId = textureId;
    }

    public String getText() {
        return text;
    }

    void setVertexCount(int vertexCount) {
        this.vertexCount = vertexCount;
    }

    public float getLength() {
        return length;
    }

    void setLength(float length) {
        this.length = length;
    }

    public float getHeight() {
        return height;
    }

    void setHeight(float height) {
        this.height = height;
    }

    public float getOpacity() {
        return opacity;
    }

    public void setOpacity(float opacity) {
        this.opacity = opacity;
    }

    public float getMaxLength() {
        return maxLength;
    }

    public void setMaxLength(float maxLength) {
        this.maxLength = maxLength;
    }

    public Vector3f getColor() {
        return color;
    }

    public void setColor(Vector3f color) {
        this.color = color;
    }
}
