package com.tagelectron.better2dengine;

import org.joml.*;

/**
 * Created by vesko on 01.07.19.
 */
public class Transform {
    private Matrix4f transform;
    private EngineObject engineObject;
    private Vector2f frameVelocity;

    public Transform(EngineObject engineObject) {
        this.engineObject = engineObject;
        transform = new Matrix4f();
        frameVelocity = new Vector2f();
    }

    public Vector2f getPosition() {
        if (engineObject.getParent() != null) {
            Matrix4f transformedPosition = multiplyWithParent(engineObject.getParent(), transform);
            Vector4f dest = new Vector4f();
            transformedPosition.transform(dest);
            return new Vector2f(dest.x, dest.y);
        } else {
            Vector4f dest = new Vector4f();
            transform.transform(dest);
            return new Vector2f(dest.x, dest.y);
        }
    }

    private Matrix4f multiplyWithParent(EngineObject parent, Matrix4f transform) {
        Matrix4f transformed = transform.mul(parent.getTransform().transform, new Matrix4f());
        if (parent.getParent() != null) {
            return multiplyWithParent(parent.getParent(), transformed);
        } else {
            return transformed;
        }
    }

    public Vector2f getLocalPosition() {
        Vector4f dest = new Vector4f();
        transform.transform(dest);
        return new Vector2f(dest.x, dest.y);
    }

    public void setPosition(Vector2f position) {
        transform.setTranslation(new Vector3f(position, 0));
    }

    public Quaternionf getRotation() {
        return transform.getUnnormalizedRotation(new Quaternionf());
    }

    public void rotate(Quaternionf rotation) {
        transform.rotate(rotation);
    }

    public Vector2f getScale() {
        Vector3f dest = new Vector3f();
        transform.getScale(dest);
        return new Vector2f(dest.x, dest.y);
    }

    public void scale(Vector2f scale) {
        transform.scale(new Vector3f(scale, 0));
    }

    public EngineObject getEngineObject() {
        return engineObject;
    }

    public Vector2f getFrameVelocity() {
        return frameVelocity;
    }

    public void setFrameVelocity(Vector2f frameVelocity) {
        this.frameVelocity = frameVelocity;
    }

    public Vector2f getForward() {
        Vector3f dest = new Vector3f();
        transform.positiveX(dest);
        return new Vector2f(dest.x, dest.y);
    }

    public Matrix4f getLocalTransform() {
        return transform;
    }

    public Matrix4f getTransform() {
        if (engineObject.getParent() != null) {
            return multiplyWithParent(engineObject.getParent(), transform);
        } else return transform;
    }

}
