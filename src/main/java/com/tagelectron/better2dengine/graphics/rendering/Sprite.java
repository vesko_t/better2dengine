package com.tagelectron.better2dengine.graphics.rendering;

import com.tagelectron.better2dengine.graphics.Texture;
import com.tagelectron.better2dengine.graphics.shaders.AbstractShader;

/**
 * Created by vesko on 01.07.19.
 */
public class Sprite {

    private Texture texture;
    private SquareModel squareModel;

    Sprite(){}

    public void render(AbstractShader shader){

    }

    public Texture getTexture() {
        return texture;
    }

    public void setTexture(Texture texture) {
        this.texture = texture;
    }

    public SquareModel getSquareModel() {
        return squareModel;
    }

    public void setSquareModel(SquareModel squareModel) {
        this.squareModel = squareModel;
    }
}
