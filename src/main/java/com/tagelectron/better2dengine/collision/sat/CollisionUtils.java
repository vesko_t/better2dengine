package com.tagelectron.better2dengine.collision.sat;

import com.tagelectron.better2dengine.collision.BoxCollider;
import com.tagelectron.better2dengine.collision.CircleCollider;
import com.tagelectron.better2dengine.collision.Collision;
import org.joml.Vector2f;

public final class CollisionUtils {
    private CollisionUtils() {
    }

    public static Collision colliding(BoxCollider boxCollider, CircleCollider circleCollider) {
        for (Vector2f vertex : boxCollider.getOrientedBoundingBox().getVertices()) {
            float distanceSquared = vertex.sub(circleCollider.getPosition(), new Vector2f()).lengthSquared();
            if (distanceSquared < circleCollider.getRadius() * circleCollider.getRadius()) {
                return new Collision();
            }
        }
        return null;
    }

}
