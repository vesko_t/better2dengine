package com.tagelectron.better2dengine.utils;

import org.joml.Vector2f;

public final class VectorUtils {

    private VectorUtils() {
    }


    public static Vector2f getPositionAfterRotation(Vector2f origin, float rotationSine, float rotationCsine) {
        return new Vector2f(origin.x * rotationCsine - origin.y * rotationSine, origin.x * rotationSine + origin.y * rotationCsine);
    }

    public static Vector2f getPositionAfterRotation(Vector2f origin, float angle) {
        return getPositionAfterRotation(origin, (float) Math.sin(angle), (float) Math.cos(angle));
    }


}
