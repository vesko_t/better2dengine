package com.tagelectron.better2dengine.collision.sat;

import com.tagelectron.better2dengine.collision.BoxCollider;
import com.tagelectron.better2dengine.collision.Collision;
import com.tagelectron.better2dengine.utils.VectorUtils;
import org.joml.Vector2f;
import org.joml.Vector3f;

import java.util.ArrayList;
import java.util.List;

public class OrientedBoundingBox {

    private Vector2f topRight;
    private Vector2f bottomRight;
    private Vector2f topLeft;
    private Vector2f bottomLeft;
    private Vector2f verticalNormal;
    private Vector2f horizontalNormal;
    private Vector2f position;

    private BoxCollider boxCollider;

    private List<Vector2f> vertices = new ArrayList<>();
    private List<Vector2f> axes = new ArrayList<>();

    public OrientedBoundingBox(BoxCollider boxCollider) {
        this.boxCollider = boxCollider;
    }

    public void update(BoxCollider boxCollider) {
        position = boxCollider.getPosition();
        float zRotation = boxCollider.getParentObject().getTransform().getRotation().getEulerAnglesXYZ(new Vector3f()).z;
        float rotSin = (float) Math.sin(zRotation);
        float rotCos = (float) Math.cos(zRotation);
        calculateVertices(rotSin, rotCos);
        calculateEdgeNormals();
    }

    private void calculateVertices(float rotSin, float rotCos) {
        topRight = VectorUtils.getPositionAfterRotation(new Vector2f(position.x + boxCollider.getSize().x, position.y + boxCollider.getSize().y), rotSin, rotCos);
        bottomRight = VectorUtils.getPositionAfterRotation(new Vector2f(position.x + boxCollider.getSize().x, position.y - boxCollider.getSize().y), rotSin, rotCos);
        topLeft = VectorUtils.getPositionAfterRotation(new Vector2f(position.x - boxCollider.getSize().x, position.y + boxCollider.getSize().y), rotSin, rotCos);
        bottomLeft = VectorUtils.getPositionAfterRotation(new Vector2f(position.x - boxCollider.getSize().x, position.y - boxCollider.getSize().y), rotSin, rotCos);
        vertices = new ArrayList<>();
        vertices.add(topRight);
        vertices.add(bottomLeft);
        vertices.add(bottomRight);
        vertices.add(topLeft);

    }

    private void calculateEdgeNormals() {
        verticalNormal = topLeft.sub(topRight, new Vector2f()).perpendicular();
        horizontalNormal = bottomRight.sub(topRight, new Vector2f()).perpendicular();
        verticalNormal.normalize();
        horizontalNormal.normalize();
        axes = new ArrayList<>();
        axes.add(verticalNormal);
        axes.add(horizontalNormal);
    }

    public BoxProjection project(Vector2f axis) {
        float min = axis.dot(vertices.get(0));
        float max = min;
        for (int j = 0; j < vertices.size(); j++) {
            float p = axis.dot(vertices.get(j));
            if (p < min) {
                min = p;
            } else if (p > max) {
                max = p;
            }
        }
        return new BoxProjection(min, max);
    }

    public Collision colliding(OrientedBoundingBox boundingBox) {
        float overlap = Float.MAX_VALUE;
        Vector2f smallest = null;
        List<Vector2f> otherAxes = boundingBox.axes;
        for (Vector2f axis : axes) {
            BoxProjection p1 = project(axis);
            BoxProjection p2 = boundingBox.project(axis);
            if (!p1.overlaps(p2)) {
                return null;
            } else {
                float o = p1.getOverlap(p2);
                if (o < overlap) {
                    // then set this one as the smallest
                    overlap = o;
                    smallest = axis;
                }
            }
        }

        for (Vector2f axis : otherAxes) {
            BoxProjection p1 = project(axis);
            BoxProjection p2 = boundingBox.project(axis);
            if (!p1.overlaps(p2)) {
                return null;
            } else {
                float o = p1.getOverlap(p2);
                if (o < overlap) {
                    // then set this one as the smallest
                    overlap = o;
                    smallest = axis;
                }
            }
        }

        return new Collision();
    }

    public BoxCollider getBoxCollider() {
        return boxCollider;
    }

    public Vector2f getTopRight() {
        return topRight;
    }

    public Vector2f getBottomRight() {
        return bottomRight;
    }

    public Vector2f getTopLeft() {
        return topLeft;
    }

    public Vector2f getBottomLeft() {
        return bottomLeft;
    }

    public Vector2f getVerticalNormal() {
        return verticalNormal;
    }

    public Vector2f getHorizontalNormal() {
        return horizontalNormal;
    }

    public Vector2f getPosition() {
        return position;
    }

    public List<Vector2f> getVertices() {
        return vertices;
    }

    public List<Vector2f> getAxes() {
        return axes;
    }
}