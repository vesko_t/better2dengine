package com.tagelectron.better2dengine.collision;

public interface ICollisionListener {

    public void onCollisionEnter(Collision collision);
}
