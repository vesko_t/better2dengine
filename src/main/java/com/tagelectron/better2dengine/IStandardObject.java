package com.tagelectron.better2dengine;

public interface IStandardObject {
    public void awake();

    public void start();

    public void update();
}
