package com.tagelectron.better2dengine.tests;

import com.tagelectron.better2dengine.EngineObject;
import com.tagelectron.better2dengine.Game;
import com.tagelectron.better2dengine.collision.BoxCollider;
import com.tagelectron.better2dengine.graphics.Texture;
import com.tagelectron.better2dengine.graphics.rendering.SpriteRenderer;
import com.tagelectron.better2dengine.graphics.rendering.SpriteCreator;
import com.tagelectron.better2dengine.io.Window;
import org.joml.Math;
import org.joml.Quaternionf;
import org.joml.Vector2f;

public class EngineTester extends Game {

    public static void main(String... args) {
        EngineTester.init(new Window(1280, 720, "Engine test", false, false));
        EngineObject engineObject = new EngineObject();
        engineObject.getTransform().scale(new Vector2f(32));
        engineObject.addComponent(new SpriteRenderer(SpriteCreator.createSprite(new Texture("/tile2.png"))));
        engineObject.addComponent(new BoxCollider(new Vector2f(1), new Vector2f()));
        Quaternionf quaternion = new Quaternionf();
        quaternion.rotateZ((float) Math.toRadians(45));
        engineObject.getTransform().rotate(quaternion);

        EngineObject engineObject1 = new EngineObject();
        engineObject1.getTransform().scale(new Vector2f(32));
        engineObject1.addComponent(new SpriteRenderer(SpriteCreator.createSprite(new Texture("/icon.png"))));
        engineObject1.getTransform().setPosition(new Vector2f(200, 0));
        startGame();
    }

}
