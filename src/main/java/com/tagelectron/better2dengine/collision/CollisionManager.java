package com.tagelectron.better2dengine.collision;

import java.util.ArrayList;
import java.util.List;

public final class CollisionManager {

    private static List<AbstractCollider> colliders;

    private CollisionManager() {
    }

    public static void addCollider(AbstractCollider collider) {
        if (colliders == null) {
            colliders = new ArrayList<>();
        }
        colliders.add(collider);
    }

    public static void removeCollider(AbstractCollider collider) {
        colliders.remove(collider);
    }

    public static void update() {
        if (colliders != null) {
            colliders.forEach(collider -> {
                collider.updateCollision(colliders);
            });
        }
    }
}
