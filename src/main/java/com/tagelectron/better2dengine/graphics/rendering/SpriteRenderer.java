package com.tagelectron.better2dengine.graphics.rendering;

import com.tagelectron.better2dengine.graphics.shaders.AbstractShader;
import com.tagelectron.better2dengine.io.Camera;
import org.joml.Matrix4f;
import org.joml.Vector3f;

/**
 * Created by vesko on 01.07.19.
 */
public class SpriteRenderer extends Renderer {

    private Sprite sprite;

    public SpriteRenderer() {
        super();
    }

    public SpriteRenderer(AbstractShader shader) {
        super(shader);
    }

    public SpriteRenderer(Sprite sprite) {
        super();
        this.sprite = sprite;
    }

    public SpriteRenderer(AbstractShader shader, Sprite sprite) {
        super(shader);
        this.sprite = sprite;
    }

    @Override
    public void destroy() {

    }

    @Override
    public void awake() {

    }

    @Override
    public void start() {

    }

    @Override
    public void update() {

    }

    @Override
    public void render() {
        if (Camera.isInView(parentObject.getTransform().getPosition())) {
            shader.bind();
            shader.setUniform("projection", Camera.getProjection());
            shader.setUniform("transform", parentObject.getTransform().getTransform());
            shader.setUniform("hasTexture", 1);
            shader.setUniform("sampler", 0);
            sprite.getTexture().bind(0);
            sprite.getSquareModel().render();
        }
    }

    public Sprite getSprite() {
        return sprite;
    }

    public void setSprite(Sprite sprite) {
        this.sprite = sprite;
    }
}
