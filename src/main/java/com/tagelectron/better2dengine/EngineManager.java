package com.tagelectron.better2dengine;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vesko on 01.07.19.
 */
public final class EngineManager {

    private static List<EngineObject> engineObjects;

    static void init() {

    }

    public static void addObject(EngineObject engineObject) {
        if (engineObjects == null) {
            engineObjects = new ArrayList<>();
        }
        engineObjects.add(engineObject);
    }

    public static void start() {
        if (engineObjects != null) {
            for (int i = 0; i < engineObjects.size(); i++) {
                engineObjects.get(i).start();
            }
        }
    }

    public static void update() {
        if (engineObjects != null) {
            for (int i = 0; i < engineObjects.size(); i++) {
                engineObjects.get(i).update();
            }
        }
    }

    public static void removeObject(EngineObject engineObject) {
        engineObjects.remove(engineObject);
    }
}
