package com.tagelectron.better2dengine.graphics.gui;


import com.tagelectron.better2dengine.graphics.Texture;
import com.tagelectron.better2dengine.graphics.text.GLText;
import org.joml.Vector2f;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vesko on 10/30/2018.
 */
public class ComplexGui extends TexturedGuiElement {

    private List<GuiElement> guiElementList = new ArrayList<>();

    private List<Vector2f> elementPositions = new ArrayList<>();

    public ComplexGui(Vector2f position, Vector2f scale, Texture texture, boolean visible) {
        super(position, scale, texture, visible);
    }

    public ComplexGui(Vector2f position, Vector2f scale, GLText text, Texture texture, boolean visible) {
        super(position, scale, text, texture, visible);
        text.setPosition(new Vector2f(1, 0));
    }

    public void addGui(GuiElement guiElement) {
        elementPositions.add(guiElement.getPosition());
        guiElement.setPosition(new Vector2f(guiElement.getPosition().x + getPosition().x, guiElement.getPosition().y + getPosition().y));
        guiElementList.add(guiElement);
    }

    public List<GuiElement> getGuiElementList() {
        return guiElementList;
    }

    @Override
    public void setPosition(Vector2f position) {
        super.setPosition(position);
        for (int i = 0; i < guiElementList.size(); i++) {
            GuiElement g = guiElementList.get(i);
            g.setPosition(new Vector2f(elementPositions.get(i).x + position.x, elementPositions.get(i).y + position.y));
        }
        if (text != null) {
            text.setPosition(position);
        }
    }

    @Override
    public void update(float delta) {
        super.update(delta);
        for (GuiElement guiElement :
                getGuiElementList()) {
            guiElement.update(0);
        }
    }

    @Override
    public void setVisible(boolean visible) {
        super.setVisible(visible);
        for (GuiElement element : guiElementList) {
            element.setVisible(visible);
        }
    }

    @Override
    public void render(Shader textureShader, Shader noTextureShader, Model model) {
        super.render(textureShader, noTextureShader, model);
        for (GuiElement guiElement : guiElementList) {
            guiElement.render(textureShader, noTextureShader, model);
        }
    }
}
