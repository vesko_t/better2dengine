package com.tagelectron.better2dengine.io;

import org.apache.commons.io.IOUtils;
import org.lwjgl.BufferUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * Created by Vesko on 1.11.2018 г..
 */
public final class FileLoader {

    private FileLoader() {
    }

    public static ByteBuffer loadResurce(String filename) throws IOException {
        byte[] bytes = IOUtils.toByteArray(FileLoader.class.getResourceAsStream(filename));

        ByteBuffer byteBuffer = BufferUtils.createByteBuffer(bytes.length);
        byteBuffer.put(bytes);
        //byteBuffer.flip();
        byteBuffer.position(0);

        return byteBuffer;
    }

    public static ByteBuffer loadFile(String filename) throws IOException {
        byte[] bytes = IOUtils.toByteArray(new FileInputStream(new File(filename)));

        ByteBuffer byteBuffer = BufferUtils.createByteBuffer(bytes.length);
        byteBuffer.put(bytes);
        byteBuffer.position(0);
        return byteBuffer;
    }
}
