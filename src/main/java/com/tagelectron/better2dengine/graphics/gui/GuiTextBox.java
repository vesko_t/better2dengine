package com.tagelectron.better2dengine.graphics.gui;

import com.tagelectron.better2dengine.graphics.Texture;
import com.tagelectron.better2dengine.graphics.text.GLText;
import org.joml.Vector2f;

/**
 * Created by vesko on 10/31/2018.
 */
public class GuiTextBox extends TexturedGuiElement {
    private Vector2f textOffset;

    public GuiTextBox(Vector2f position, Vector2f scale, GLText text, Texture texture, boolean visible, Vector2f textOffset) {
        super(position, scale, text, texture, visible);
        this.textOffset = textOffset;
        Vector2f textPosition = new Vector2f();

        position.add(textOffset, textPosition);
        text.setPosition(textPosition);
    }

    @Override
    public void setPosition(Vector2f position) {
        super.setPosition(position);
        Vector2f textPosition = new Vector2f();

        position.add(textOffset, textPosition);
        text.setPosition(textPosition);
    }

    public void setText(String text) {
        this.text.setText(text);
    }
}
