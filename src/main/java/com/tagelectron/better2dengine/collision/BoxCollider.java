package com.tagelectron.better2dengine.collision;

import com.tagelectron.better2dengine.EngineObject;
import com.tagelectron.better2dengine.collision.sat.CollisionUtils;
import com.tagelectron.better2dengine.collision.sat.OrientedBoundingBox;
import com.tagelectron.better2dengine.graphics.Texture;
import com.tagelectron.better2dengine.graphics.rendering.SpriteRenderer;
import com.tagelectron.better2dengine.graphics.rendering.SpriteCreator;
import org.joml.Vector2f;

import java.util.List;

public class BoxCollider extends AbstractCollider {

    private Vector2f size;
    private Vector2f relativeSize;
    private OrientedBoundingBox orientedBoundingBox;
    private EngineObject testObject;

    public BoxCollider(Vector2f relativeSize, Vector2f relativePosition) {
        super(relativePosition);
        this.relativeSize = relativeSize;
        orientedBoundingBox = new OrientedBoundingBox(this);
        testObject = new EngineObject();
        testObject.addComponent(new SpriteRenderer(SpriteCreator.createSprite(new Texture("/tile2.png"))));
        testObject.getTransform().setScale(new Vector2f(0.5f));
    }


    @Override
    protected void checkCollision(AbstractCollider collider) {
        if (collider instanceof BoxCollider) {
            checkCollision((BoxCollider) collider);
            if (orientedBoundingBox.colliding(((BoxCollider) collider).orientedBoundingBox) != null) {
                //System.out.println("Colliding");
            } else {
                //System.out.println("No collision");
            }
        } else if (collider instanceof CircleCollider) {
            if (CollisionUtils.colliding(this, (CircleCollider) collider) != null) {
                //System.out.println("Colliding");
            } else {
                //System.out.println("No collision");
            }
        }
    }

    @Override
    public void destroy() {

    }

    @Override
    public void awake() {

    }

    @Override
    public void start() {

    }

    @Override
    public void update() {
        super.update();
        size = relativeSize.mul(parentObject.getTransform().getRenderScale(), new Vector2f());
        orientedBoundingBox.update(this);
        testObject.getTransform().setPosition(orientedBoundingBox.getBottomLeft());
        testObject.getTransform().setRotation(parentObject.getTransform().getRotation());
    }

    private Collision checkCollision(BoxCollider boxCollider) {
        return null;
    }

    @Override
    public void onCollisionEnter(Collision collision) {

    }


    @Override
    public void setParentObject(EngineObject engineObject) {
        super.setParentObject(engineObject);
        position = relativePosition.add(parentObject.getTransform().getPosition(), new Vector2f());
    }

    public void setSize(Vector2f size) {
        this.size = size;
    }

    public void setRelativePosition(Vector2f relativePosition) {
        this.relativePosition = relativePosition;
    }

    public Vector2f getSize() {
        return size;
    }

    public OrientedBoundingBox getOrientedBoundingBox() {
        return orientedBoundingBox;
    }
}
