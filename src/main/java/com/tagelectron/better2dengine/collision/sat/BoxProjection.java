package com.tagelectron.better2dengine.collision.sat;

public class BoxProjection {

    private float min;
    private float max;

    public BoxProjection() {
    }

    public BoxProjection(float min, float max) {
        this.min = min;
        this.max = max;
    }

    public boolean overlaps(BoxProjection boxProjection) {
        if (max < boxProjection.min || boxProjection.max < min)
            return false; //no overlap
        else
            return true; //yes overlap
    }

    public float getOverlap(BoxProjection boxProjection) {
        if (!overlaps(boxProjection)) {
            return 0;
        }

        if (max > boxProjection.min) {
            return Math.abs(max - boxProjection.min);
        } else if (min > boxProjection.max) {
            return Math.abs(min - boxProjection.max);
        } else {
            System.err.println("Bad case in getOverlap!");
            return 0;
        }
    }


    public float getMin() {
        return min;
    }

    public void setMin(float min) {
        this.min = min;
    }

    public float getMax() {
        return max;
    }

    public void setMax(float max) {
        this.max = max;
    }
}
