package com.tagelectron.better2dengine.graphics.gui;

import com.tagelectron.engine.audio.AudioManager;
import com.tagelectron.engine.audio.AudioSource;
import com.tagelectron.engine.entities.Updateable;
import com.tagelectron.engine.graphics.Texture;
import com.tagelectron.engine.io.Input;
import com.tagelectron.engine.text.GLText;
import org.joml.Vector2f;

public class GuiButton extends TexturedGuiElement implements Updateable {

    private GuiListener listener;

    private Texture hoverTexture;

    private Texture pressedTexture;

    private Texture defaultTexture;

    private String command;

    private AudioSource audioSource;

    private int clickSound = -1;

    private int releaseSound = -1;

    public GuiButton(Vector2f position, Vector2f scale, boolean visible, GLText text, Texture texture) {
        super(position, scale, texture, visible);
        this.text = text;
        defaultTexture = texture;
        text.setPosition(new Vector2f((position.x + scale.x) - text.getLength() / 2.0f, (position.y + scale.y) - text.getHeight() / 2.0f));
        if (AudioManager.isInitialized()) {
            audioSource = new AudioSource();
        }
    }

    public GuiButton(Vector2f position, Vector2f scale, boolean visible, GLText text, Texture texture, String command) {
        super(position, scale, texture, visible);
        this.command = command;
        this.text = text;
        defaultTexture = texture;
        text.setPosition(new Vector2f((position.x + scale.x) - text.getLength() / 2.0f, (position.y + scale.y) - text.getHeight() / 2.0f));
        if (AudioManager.isInitialized()) {
            audioSource = new AudioSource();
            audioSource.setLooping(false);
        }
    }

    public AudioSource getAudioSource() {
        return audioSource;
    }

    public void setHoverTexture(Texture hoverTexture) {
        this.hoverTexture = hoverTexture;
    }

    public void setPressedTexture(Texture pressedTexture) {
        this.pressedTexture = pressedTexture;
    }

    public void setListener(GuiListener listener) {
        this.listener = listener;
    }

    public GuiListener getListener() {
        return listener;
    }

    public void setClickSound(int clickSound) {
        this.clickSound = clickSound;
    }

    public void setReleaseSound(int releaseSound) {
        this.releaseSound = releaseSound;
    }

    @Override
    public void update(float delta) {
        super.update(delta);
        if (isMouseOver()) {
            if (hoverTexture != null && !Input.isMouseButtonDown(0)) {
                texture = hoverTexture;
            }
            if (Input.isMouseButtonPressed(0)) {
                if (audioSource != null && clickSound != -1) {
                    audioSource.start(clickSound);
                }
            }
            if (Input.isMouseButtonDown(0)) {
                if (pressedTexture != null) {
                    texture = pressedTexture;
                }
            }
            if (Input.isMouseButtonReleased(0)) {
                if (listener != null) {
                    if (audioSource != null && releaseSound != -1) {
                        audioSource.start(releaseSound);
                    }
                    listener.onAction(command);
                }
            }
        } else {
            texture = defaultTexture;
        }
    }

    @Override
    public void constantUpdate(float delta) {

    }

    public void setCommand(String command) {
        this.command = command;
    }
}
