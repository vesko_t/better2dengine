package com.tagelectron.better2dengine.graphics.gui;

import com.tagelectron.engine.graphics.Model;
import com.tagelectron.engine.graphics.Texture;
import com.tagelectron.engine.rendering.Shader;
import com.tagelectron.engine.text.GLText;
import org.joml.Vector2f;

public class TexturedGuiElement extends GuiElement {

    protected Texture texture;

    public TexturedGuiElement(Vector2f position, Vector2f scale, GLText text, Texture texture, boolean visible) {
        super(position, scale, text, visible);
        this.texture = texture;
    }

    public TexturedGuiElement(Vector2f position, Vector2f scale, Texture texture, boolean visible) {
        super(position, scale, visible);
        this.texture = texture;
    }

    public Texture getTexture() {
        return texture;
    }

    public void setTexture(Texture texture) {
        this.texture = texture;
    }

    @Override
    public void render(Shader textureShader, Shader noTextureShader, Model model) {
        texture.bind(0);
        textureShader.bind();
        textureShader.setUniform("sampler", 0);
        textureShader.setUniform("model", modelMatrix);
        textureShader.setUniform("projection", GuiManager.getProjection());
        model.render();
        if (text != null) {
            text.render();
        }
    }
}
