package com.tagelectron.better2dengine.graphics.shaders;

import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.joml.Vector3f;
import org.joml.Vector4f;
import org.lwjgl.BufferUtils;

import java.nio.FloatBuffer;
import java.util.List;

import static org.lwjgl.opengl.GL20.*;

/**
 * Created by vesko on 01.07.19.
 */
public abstract class AbstractShader {

    protected int program;

    protected FloatBuffer matrixData;

    public AbstractShader() {
        matrixData = BufferUtils.createFloatBuffer(16);
    }

    public void bindAttributeLocation(int id, String name){
        glBindAttribLocation(program, id, name);
    }

    public void setUniform(String name, int value) {
        int location = glGetUniformLocation(program, name);
        if (location != -1)
            glUniform1i(location, value);
    }

    public void setUniform(String name, float value) {
        int location = glGetUniformLocation(program, name);
        if (location != -1)
            glUniform1f(location, value);
    }

    public void setUniform(String name, Matrix4f value) {
        value.get(matrixData);
        int location = glGetUniformLocation(program, name);
        if (location != -1) {
            glUniformMatrix4fv(location, false, matrixData);
        }
    }

    public void setUniform(String name, Vector3f value) {
        int location = glGetUniformLocation(program, name);
        if (location != -1) {
            glUniform3f(location, value.x, value.y, value.z);
        }
    }

    public void setUniform(String name, Vector2f value) {
        int location = glGetUniformLocation(program, name);
        if (location != -1) {
            glUniform2f(location, value.x, value.y);
        }
    }

    public void setUniform(String name, Vector4f value) {
        int location = glGetUniformLocation(program, name);
        if (location != -1) {
            glUniform4f(location, value.x, value.y, value.z, value.w);
        }
    }

    public void setUniform(String name, float[] values) {
        int location = glGetUniformLocation(program, name);
        if (location != -1) {
            glUniform2fv(location, values);
        }
    }

    public void setUniform(String name, List<Vector3f> values) {
        int location = glGetUniformLocation(program, name);
        if (location != -1) {
            float[] data = new float[values.size() * 2];

            for (int i = 1; i < values.size(); i += 2) {
                data[i - 1] = values.get(i - 1).x;
                data[i] = values.get(i - 1).y;
            }

            glUniform2fv(location, data);
        }
    }

    void setProgram(int program) {
        this.program = program;
    }

    public void bind() {
        glUseProgram(program);
    }
}
