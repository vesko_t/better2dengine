package com.tagelectron.better2dengine.graphics.text;

import com.tagelectron.better2dengine.io.FileLoader;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.stb.STBTTAlignedQuad;
import org.lwjgl.stb.STBTTBakedChar;
import org.lwjgl.stb.STBTTFontinfo;
import org.lwjgl.system.MemoryStack;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.GL_TEXTURE0;
import static org.lwjgl.opengl.GL13.glActiveTexture;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.stb.STBTruetype.*;
import static org.lwjgl.system.MemoryStack.stackPush;

public class GLTrueTypeFont {

    private static final int CHAR_AMT = 256;

    private float scale;
    private int ascent;
    private int descent;
    private int lineGap;
    private STBTTBakedChar.Buffer cdata;
    private float fontSize;
    private final int bitmapWidth;
    private final int bitmapHeight;
    private int textureId;

    public GLTrueTypeFont(String fontFile, float fontSize, int bitmapWidth, int bitmapHeight) {
        this.fontSize = fontSize;
        this.bitmapWidth = bitmapWidth;
        this.bitmapHeight = bitmapHeight;
        try {
            cdata = STBTTBakedChar.malloc(CHAR_AMT);

            ByteBuffer ttf = FileLoader.loadResurce(fontFile);

            STBTTFontinfo info = STBTTFontinfo.create();

            if (!stbtt_InitFont(info, ttf)) {
                throw new IllegalStateException("Failed to initialize font information.");
            }

            try (MemoryStack stack = stackPush()) {
                IntBuffer pAscent = stack.mallocInt(1);
                IntBuffer pDescent = stack.mallocInt(1);
                IntBuffer pLineGap = stack.mallocInt(1);

                stbtt_GetFontVMetrics(info, pAscent, pDescent, pLineGap);

                ascent = pAscent.get(0);
                descent = pDescent.get(0);
                lineGap = pLineGap.get(0);
            }
            scale = stbtt_ScaleForPixelHeight(info, fontSize);

            ByteBuffer bitmap = BufferUtils.createByteBuffer(bitmapWidth * bitmapHeight);
            stbtt_BakeFontBitmap(ttf, fontSize, bitmap, bitmapWidth, bitmapHeight, 0, cdata);

            glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
            textureId = glGenTextures();
            glBindTexture(GL_TEXTURE_2D, textureId);
            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
            glTexImage2D(GL_TEXTURE_2D, 0, GL11.GL_RED, bitmapWidth, bitmapHeight, 0, GL11.GL_RED, GL_UNSIGNED_BYTE, bitmap);
            glPixelStorei(GL_UNPACK_ALIGNMENT, 4);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public GLTrueTypeFont(String fontFile, float fontSize) {
        this(fontFile, fontSize, 512, 512);
    }

    public void bindAtlas() {
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, textureId);
    }

    public void generateVbos(GLText glTextObject, float lineLength) {
        String text = glTextObject.getText();

        int count = text.length() * 4;
        int verticesId = glGenBuffers();
        int textureCoordinates = glGenBuffers();
        try (MemoryStack stack = stackPush()) {

            FloatBuffer vert = stack.mallocFloat(text.length() * 8);
            FloatBuffer tex = stack.mallocFloat(text.length() * 8);

            float length = 0;
            int lineNum = 0;
            char[] charArray = text.toCharArray();
            for (int i = 0; i < charArray.length; i++) {
                char c = charArray[i];
                STBTTAlignedQuad stbQuad = STBTTAlignedQuad.create();
                float[] x = new float[1];
                float[] y = new float[1];
                stbtt_GetBakedQuad(cdata, bitmapWidth, bitmapHeight, c, x, y, stbQuad, true);

                if (lineLength != -1) {
                    if (c == '\n' || (lineLength - (lineLength / 8) < length) && charArray[i] == 32) {
                        lineNum++;
                        length = 0;
                        continue;
                    }
                }

                float[] vertices = new float[]{
                        length + stbQuad.x0(), stbQuad.y0() + lineNum * ((ascent - descent + lineGap) * scale),
                        length + stbQuad.x1(), stbQuad.y0() + lineNum * ((ascent - descent + lineGap) * scale),
                        length + stbQuad.x1(), stbQuad.y1() + lineNum * ((ascent - descent + lineGap) * scale),
                        length + stbQuad.x0(), stbQuad.y1() + lineNum * ((ascent - descent + lineGap) * scale)
                };

                float[] textures = new float[]{
                        stbQuad.s0(), stbQuad.t0(),
                        stbQuad.s1(), stbQuad.t0(),
                        stbQuad.s1(), stbQuad.t1(),
                        stbQuad.s0(), stbQuad.t1()
                };
                tex.put(textures);
                vert.put(vertices);
                length += x[0];
            }

            vert.position(0);
            tex.position(0);

            glBindBuffer(GL_ARRAY_BUFFER, verticesId);
            glBufferData(GL_ARRAY_BUFFER, vert, GL_STATIC_DRAW);

            glBindBuffer(GL_ARRAY_BUFFER, textureCoordinates);
            glBufferData(GL_ARRAY_BUFFER, tex, GL_STATIC_DRAW);
            glBindBuffer(GL_ARRAY_BUFFER, 0);

            glTextObject.setTextureId(textureCoordinates);
            glTextObject.setVertexId(verticesId);
            glTextObject.setVertexCount(count);
            glTextObject.setLength(length);
            glTextObject.setHeight(fontSize);
        }
    }


    public float getFontSize() {
        return fontSize;
    }
}
