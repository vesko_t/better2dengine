package com.tagelectron.better2dengine.graphics.gui;

import com.tagelectron.engine.text.GLText;
import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.joml.Vector3f;
import org.joml.Vector4f;

public class GuiBar extends ColoredGuiElement {

    private float maxLength;

    private Vector2f originalPosition;

    public GuiBar(Vector2f position, Vector2f scale, Vector4f color, boolean visible) {
        super(position, scale, color, visible);
        maxLength = scale.x * 2;
        originalPosition = new Vector2f(position.x);
    }

    public GuiBar(Vector2f position, Vector2f scale, GLText text, Vector4f color, boolean visible) {
        super(position, scale, text, color, visible);
        maxLength = scale.x * 2;
        originalPosition = new Vector2f(position.x);
    }

    public void setValue(float valuePercent) {
        if (valuePercent >= 0 && valuePercent <= 100) {
            float currentLenght = maxLength * (valuePercent / 100.0f);
            scale.x = currentLenght / 2;
            modelMatrix = new Matrix4f();
            modelMatrix.scale(new Vector3f(scale, 0));
            setPosition(originalPosition);
        }
    }

    @Override
    public void setPosition(Vector2f position) {
        super.setPosition(position);
        originalPosition = position;
    }
}
