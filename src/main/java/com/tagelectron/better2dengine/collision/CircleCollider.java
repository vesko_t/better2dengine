package com.tagelectron.better2dengine.collision;

import com.tagelectron.better2dengine.Transform;
import com.tagelectron.better2dengine.collision.sat.CollisionUtils;
import org.joml.Vector2f;

import java.util.List;

public class CircleCollider extends AbstractCollider {

    private float radius;

    public CircleCollider(Vector2f relativePosition, float radius) {
        super(relativePosition);
        this.radius = Transform.defaultScale * radius;
    }

    @Override
    protected void checkCollision(AbstractCollider collider) {
        if (collider instanceof CircleCollider) {
            if (colliding((CircleCollider) collider) != null) {
                //System.out.println("Colliding");
            } else {
                //System.out.println("No collision");
            }
        } else if (collider instanceof BoxCollider) {
            if (CollisionUtils.colliding((BoxCollider) collider, this) != null) {
                //System.out.println("Colliding");
            } else {
                //System.out.println("No collision");
            }
        }
    }

    @Override
    public void destroy() {

    }

    @Override
    public void awake() {

    }

    @Override
    public void start() {

    }

    @Override
    public void update() {
        super.update();
    }

    @Override
    public void onCollisionEnter(Collision collision) {

    }

    private Collision colliding(CircleCollider other) {
        if (position.sub(other.position, new Vector2f()).lengthSquared() < radius * radius + other.getRadius() * other.getRadius()) {
            return new Collision();
        }
        return null;
    }

    public float getRadius() {
        return radius;
    }
}
