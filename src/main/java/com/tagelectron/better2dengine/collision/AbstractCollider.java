package com.tagelectron.better2dengine.collision;

import com.tagelectron.better2dengine.AbstractComponent;
import org.joml.Vector2f;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractCollider extends AbstractComponent implements ICollisionListener {

    protected Vector2f position;
    protected Vector2f relativePosition;
    protected List<AbstractCollider> checkedColliders = new ArrayList<>();

    private List<ICollisionListener> collisionListeners;

    public AbstractCollider(Vector2f relativePosition) {
        this.relativePosition = relativePosition;
        collisionListeners = new ArrayList<>();
        collisionListeners.add(this);
        CollisionManager.addCollider(this);
    }

    @Override
    public void update() {
        position = relativePosition.add(parentObject.getTransform().getPosition(), new Vector2f());
        checkedColliders.clear();
    }

    public void addCollisionListener(ICollisionListener collisionListener) {
        collisionListeners.add(collisionListener);
    }

    public void updateCollision(List<AbstractCollider> colliders) {
        for (AbstractCollider abstractCollider : colliders) {
            if (!abstractCollider.equals(this) && !abstractCollider.checkedColliders.contains(this)) {
                checkCollision(abstractCollider);
                checkedColliders.add(abstractCollider);
            }
        }
    }

    public Vector2f getPosition() {
        return position;
    }

    protected abstract void checkCollision(AbstractCollider abstractCollider);

}
