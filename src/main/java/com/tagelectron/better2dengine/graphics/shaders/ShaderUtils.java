package com.tagelectron.better2dengine.graphics.shaders;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import static org.lwjgl.opengl.GL20.*;

public final class ShaderUtils {

    private ShaderUtils() {
    }

    public static AbstractShader createShader(String vertexCode, String fragmentCode){
        StandardShader standardShader = new StandardShader();

        int program = glCreateProgram();

        int vs = glCreateShader(GL_VERTEX_SHADER);
        glShaderSource(vs, vertexCode);
        glCompileShader(vs);
        if (glGetShaderi(vs, GL_COMPILE_STATUS) != 1) {
            System.err.println("Vertex error " + glGetShaderInfoLog(vs));
            System.exit(1);
        }

        int fs = glCreateShader(GL_FRAGMENT_SHADER);
        glShaderSource(fs, fragmentCode);
        glCompileShader(fs);
        if (glGetShaderi(fs, GL_COMPILE_STATUS) != 1) {
            System.err.println("Fragment error " + glGetShaderInfoLog(fs));
            System.exit(1);
        }

        glAttachShader(program, vs);
        glAttachShader(program, fs);

        glLinkProgram(program);
        if (glGetProgrami(program, GL_LINK_STATUS) != 1) {
            System.err.println("Link error " + glGetProgramInfoLog(program));
            System.exit(1);
        }

        glValidateProgram(program);
        if (glGetProgrami(program, GL_VALIDATE_STATUS) != 1) {
            System.err.println("Validate error " + glGetProgramInfoLog(program));
            System.exit(1);
        }

        standardShader.setProgram(program);
        return standardShader;
    }

    public static String loadShaderCode(String relativeFilePath) {
        BufferedReader reader;
        StringBuilder output = new StringBuilder();

        InputStream is = AbstractShader.class.getResourceAsStream(relativeFilePath);


        try {
            reader = new BufferedReader(new InputStreamReader(is));

            String line;
            while ((line = reader.readLine()) != null) {
                output.append(line).append("\n");
            }

            reader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return output.toString();
    }

}
