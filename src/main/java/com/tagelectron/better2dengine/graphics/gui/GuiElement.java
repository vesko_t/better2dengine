package com.tagelectron.better2dengine.graphics.gui;

import com.tagelectron.engine.entities.Updateable;
import com.tagelectron.engine.graphics.Model;
import com.tagelectron.engine.io.Input;
import com.tagelectron.engine.rendering.Shader;
import com.tagelectron.engine.text.GLText;
import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.joml.Vector3f;

public abstract class GuiElement implements Updateable {

    protected Matrix4f modelMatrix = new Matrix4f().identity();

    protected Vector2f position;

    protected Vector2f scale;

    protected GLText text;

    protected boolean visible;

    public GuiElement(Vector2f position, Vector2f scale, boolean visible) {
        this(position, scale, null, visible);
    }

    public GuiElement(Vector2f position, Vector2f scale, GLText text, boolean visible) {
        this.text = text;
        this.visible = visible;
        this.position = new Vector2f(position.x + scale.x, position.y + scale.y);
        this.scale = scale;
        if (text != null)
            this.text.setPosition(new Vector2f(position));
        modelMatrix.scale(new Vector3f(this.scale, 0));
        modelMatrix.setTranslation(new Vector3f(this.position, 0));
    }

    public boolean isMouseOver() {
        Vector2f mousePos = Input.getMousePositionScreen();

        return mousePos.x > position.x - scale.x && mousePos.x < position.x + scale.x && mousePos.y > position.y - scale.y && mousePos.y < position.y + scale.y;
    }

    @Override
    public boolean isToBeRemoved() {
        return !visible;
    }

    @Override
    public void setToBeRemoved(boolean toBeRemoved) {
        this.visible = !toBeRemoved;
    }

    @Override
    public void update(float delta) {
        modelMatrix.setTranslation(new Vector3f(position, 0));
        Input.setMouseOverGui(isMouseOver());
    }

    @Override
    public void constantUpdate(float delta) {

    }

    public abstract void render(Shader textureShader, Shader noTextureShader, Model model);

    @Override
    public Vector2f getPosition() {
        return new Vector2f(position.x - scale.x, position.y - scale.y);
    }

    public void setPosition(Vector2f position) {
        this.position = new Vector2f(position.x + scale.x, position.y + scale.y);
        if (text != null) {
            text.setPosition(position);
        }
        modelMatrix.setTranslation(new Vector3f(this.position, 0));
    }

    public Vector2f getScale() {
        return scale;
    }

    public void setScale(Vector2f scale) {
        this.scale = scale;
        modelMatrix.scale(new Vector3f(scale, 0));
    }

    public GLText getText() {
        return text;
    }

    public void setText(GLText text) {
        this.text = text;
    }

    public Matrix4f getModelMatrix() {
        return modelMatrix;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }
}
