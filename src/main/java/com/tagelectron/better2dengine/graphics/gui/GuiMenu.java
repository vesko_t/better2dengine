package com.tagelectron.better2dengine.graphics.gui;

import com.tagelectron.engine.graphics.Background;
import com.tagelectron.engine.graphics.Model;
import com.tagelectron.engine.rendering.Shader;
import org.joml.Vector2f;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vesko on 13.11.2018 г..
 */
public class GuiMenu extends GuiElement {

    private Background background;
    private List<GuiElement> guiElements = new ArrayList<>();

    public GuiMenu(boolean visible) {
        super(new Vector2f(), new Vector2f(), visible);
    }

    public GuiMenu(boolean visible, Background background) {
        super(new Vector2f(), new Vector2f(), visible);
        this.background = background;
    }

    @Override
    public void update(float delta) {
        super.update(delta);
        for (GuiElement guiElement : guiElements) {
            guiElement.update(0);
        }
    }

    @Override
    public void render(Shader textureShader, Shader noTextureShader, Model model) {
        if (visible) {
            if (background != null)
                background.render();
            for (GuiElement guiElement : guiElements) {
                guiElement.render(textureShader, noTextureShader, model);
            }
        }
    }

    public List<GuiElement> getGuiElements() {
        return guiElements;
    }
}
