package com.tagelectron.better2dengine.graphics.rendering;

import com.tagelectron.better2dengine.graphics.utils.GraphicsUtils;

import static org.lwjgl.opengl.GL11.GL_FLOAT;
import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.glDrawArrays;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL15.GL_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL20.glDisableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glVertexAttribPointer;

/**
 * Created by vesko on 01.07.19.
 */
public class SquareModel {
    private int vId, tId, iId, count;


    public SquareModel() {

        float[] vertices = new float[]{
                -1, 1, // top left corner
                1, 1,  // top right corner
                -1, -1, // bottom left corner
                1, -1, // bottom right corner
        };

        float[] textures = new float[]{
                0, 0,
                1, 0,
                0, 1,
                1, 1
        };

        int[] indices = new int[]{
                1, 0, 2, // first triangle (bottom left - top left - top right)
                1, 2, 3
        };

        fillBuffers(vertices, textures, indices);
    }

    public SquareModel(float[] textures) {

        float[] vertices = new float[]{
                -1, -1, // bottom left corner
                -1, 1, // top left corner
                1, 1,  // top right corner
                1, -1, // bottom right corner
        };

        int[] indices = new int[]{
                0, 1, 2, // first triangle (bottom left - top left - top right)
                0, 2, 3
        };

        fillBuffers(vertices, textures, indices);
    }

    private void fillBuffers(float[] vertices, float[] texCoords, int[] indices) {
        count = indices.length;

        tId = GraphicsUtils.createVbo(texCoords);

        vId = GraphicsUtils.createVbo(vertices);

        iId = GraphicsUtils.createVbo(indices);
    }

    public void render() {

        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(1);

        glBindBuffer(GL_ARRAY_BUFFER, vId);
        glVertexAttribPointer(0, 2, GL_FLOAT, false, 0, 0);

        glBindBuffer(GL_ARRAY_BUFFER, tId);
        glVertexAttribPointer(1, 2, GL_FLOAT, false, 0, 0);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, iId);

        glDrawElements(GL_TRIANGLES, count, GL_UNSIGNED_INT, 0);

        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

        glDisableVertexAttribArray(0);
        glDisableVertexAttribArray(1);
    }

}
