package com.tagelectron.better2dengine.collision;

import com.tagelectron.better2dengine.EngineObject;
import org.joml.Vector2f;

public class Collision {

    private EngineObject engineObject;

    private Vector2f intersection;

    private Vector2f mtv;

    public Collision() {
    }

    public Collision(EngineObject engineObject, Vector2f intersection, Vector2f mtv) {
        this.engineObject = engineObject;
        this.intersection = intersection;
        this.mtv = mtv;
    }

    public EngineObject getEngineObject() {
        return engineObject;
    }

    public void setEngineObject(EngineObject engineObject) {
        this.engineObject = engineObject;
    }

    public Vector2f getIntersection() {
        return intersection;
    }

    public void setIntersection(Vector2f intersection) {
        this.intersection = intersection;
    }

    public Vector2f getMtv() {
        return mtv;
    }

    public void setMtv(Vector2f mtv) {
        this.mtv = mtv;
    }
}
