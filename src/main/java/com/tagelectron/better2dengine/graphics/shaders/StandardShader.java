package com.tagelectron.better2dengine.graphics.shaders;

import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL20.glGetProgramInfoLog;

public class StandardShader extends AbstractShader {

    private static final String vertexCode = "#version 120\n" +
            "\n" +
            "attribute vec2 textures;\n" +
            "attribute vec2 vertices;\n" +
            "\n" +
            "varying vec2 texCoords;\n" +
            "\n" +
            "uniform mat4 projection;\n" +
            "\n" +
            "uniform mat4 model;\n" +
            "\n" +
            "void main() {\n" +
            "\n" +
            "    texCoords = textures;\n" +
            "\n" +
            "    gl_Position = projection * model * vec4(vertices,0,1);\n" +
            "}\n";

    private static final String fragmentCode = "#version 120\n" +
            "\n" +
            "uniform sampler2D sampler;\n" +
            "uniform int hasTexture;\n" +
            "uniform vec4 color;\n" +
            "varying vec2 texCoords;\n" +
            "\n" +
            "void main() {\n" +
            "    vec4 texture;\n" +
            "    if (hasTexture == 1){\n" +
            "        texture = texture2D(sampler, texCoords);\n" +
            "        texture *= color;\n" +
            "    }else {\n" +
            "        texture = color;\n" +
            "    }\n" +
            "    gl_FragColor = texture;\n" +
            "}";

    public StandardShader() {
        program = glCreateProgram();

        int vs = glCreateShader(GL_VERTEX_SHADER);
        glShaderSource(vs, vertexCode);
        glCompileShader(vs);
        if (glGetShaderi(vs, GL_COMPILE_STATUS) != 1) {
            System.err.println("Vertex error " + glGetShaderInfoLog(vs));
            System.exit(1);
        }

        int fs = glCreateShader(GL_FRAGMENT_SHADER);
        glShaderSource(fs, fragmentCode);
        glCompileShader(fs);
        if (glGetShaderi(fs, GL_COMPILE_STATUS) != 1) {
            System.err.println("Fragment error " + glGetShaderInfoLog(fs));
            System.exit(1);
        }

        glAttachShader(program, vs);
        glAttachShader(program, fs);

        glLinkProgram(program);
        if (glGetProgrami(program, GL_LINK_STATUS) != 1) {
            System.err.println("Link error " + glGetProgramInfoLog(program));
            System.exit(1);
        }

        glValidateProgram(program);
        if (glGetProgrami(program, GL_VALIDATE_STATUS) != 1) {
            System.err.println("Validate error " + glGetProgramInfoLog(program));
            System.exit(1);
        }
    }

}
