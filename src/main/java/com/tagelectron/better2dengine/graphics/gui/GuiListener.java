package com.tagelectron.better2dengine.graphics.gui;

public interface GuiListener {

    public void onAction(String command);

}
