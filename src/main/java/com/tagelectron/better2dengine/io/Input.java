package com.tagelectron.better2dengine.io;


import static org.lwjgl.glfw.GLFW.*;

public final class Input {
    private static boolean[] keys;

    private static boolean[] buttons;

    private static boolean mouseOverGui;

    private Input(){}

    static void init() {
        keys = new boolean[GLFW_KEY_LAST];
        for (int i = GLFW_KEY_SPACE; i < GLFW_KEY_LAST; i++) {
            keys[i] = false;
        }
        buttons = new boolean[3];
        for (int i = 0; i < 3; i++) {
            buttons[i] = false;
        }
    }

    public static boolean isKeyDown(int key) {
        return glfwGetKey(Window.window, key) == 1;
    }


    public static boolean isMouseButtonDown(int button) {
        return glfwGetMouseButton(Window.window, button) == 1;
    }

    public static void update() {
        for (int i = GLFW_KEY_SPACE; i < GLFW_KEY_LAST; i++) {
            keys[i] = isKeyDown(i);
        }
        for (int i = 0; i < 3; i++) {
            buttons[i] = isMouseButtonDown(i);
        }
    }

/*    public static Vector2f getMousePositionScreen() {
        double xPos[] = new double[1];
        double yPos[] = new double[1];
        glfwGetCursorPos(Window.window, xPos, yPos);

        float xOffset = (float) Camera.getWidth() / (float) Window.getWidth();
        float yOffset = (float) Camera.getHeight() / (float) Window.getHeight();
        return new Vector2f((float) xPos[0] * xOffset, (float) yPos[0] * yOffset);
    }

    public static Vector2f getMousePositionCamera() {

        return new Vector2f(getMousePositionScreen().x - Camera.getWidth() / 2.0f, (getMousePositionScreen().y - Camera.getHeight() / 2.0f) * -1);
    }

    public static Vector2f getMousePositionWorld() {
        Vector2f pos = new Vector2f();
        Camera.getPosition().mul(-1, pos);

        return pos.add(Input.getMousePositionCamera());
    }*/

    public static boolean isMouseButtonPressed(int button) {
        return (isMouseButtonDown(button) && !buttons[button]);
    }

    public static boolean isKeyPressed(int key) {
        return (isKeyDown(key) && !keys[key]);
    }

    public static boolean isMouseButtonReleased(int button) {
        return (!isMouseButtonDown(button) && buttons[button]);
    }

    public static boolean isKeyReleased(int key) {
        return (!isKeyDown(key) && keys[key]);
    }

    public static boolean isMouseOverGui() {
        return mouseOverGui;
    }

    public static void setMouseOverGui(boolean mouseOverGui) {
        Input.mouseOverGui = mouseOverGui;
    }
}
