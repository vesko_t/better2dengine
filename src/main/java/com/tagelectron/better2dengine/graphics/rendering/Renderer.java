package com.tagelectron.better2dengine.graphics.rendering;

import com.tagelectron.better2dengine.AbstractComponent;
import com.tagelectron.better2dengine.graphics.shaders.AbstractShader;
import org.joml.Matrix4f;

/**
 * Created by vesko on 01.07.19.
 */
public abstract class Renderer extends AbstractComponent {

    protected AbstractShader shader;

    public Renderer() {
        this(GraphicsRenderer.getStandardShader());
    }

    public Renderer(AbstractShader shader) {
        GraphicsRenderer.addRenderer(this);
        this.shader = shader;
    }

    public abstract void render();
}
