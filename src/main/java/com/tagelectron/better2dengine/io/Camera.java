package com.tagelectron.better2dengine.io;

import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.joml.Vector3f;

public class Camera {
    private static Matrix4f projection;
    private static int width;
    private static int height;
    private static Vector2f position;
    private static Vector2f scale;

    public Camera(int width, int height) {
        Camera.width = width;
        Camera.height = height;
        position = new Vector2f(0);
        projection = new Matrix4f().ortho2D(-width / 2.0f, width / 2.0f, -height / 2.0f, height / 2.0f);
        scale = new Vector2f(1);
    }

    public static Vector2f getPosition() {
        return position;
    }

    public static void setPosition(Vector2f position) {
        Camera.position = position;
    }

    public static Matrix4f getProjection() {
        return projection.scale(new Vector3f(scale, 0)).translate(new Vector3f(position, 0), new Matrix4f());
    }

    public static int getWidth() {
        return width;
    }

    public static int getHeight() {
        return height;
    }

    public static void setScale(Vector2f scale) {
        Camera.scale = scale;
    }

    public static Vector2f getScale() {
        return scale;
    }

    public static boolean isInView(Vector2f position) {
        return position.x > Camera.position.x - Camera.width / 2.0f &&
                position.x < Camera.position.x + Camera.width / 2.0f &&
                position.y > Camera.position.y - Camera.height / 2.0f &&
                position.y < Camera.position.y + Camera.height / 2.0f;
    }
}
