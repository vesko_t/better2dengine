package com.tagelectron.better2dengine;

import org.joml.Quaternionf;
import org.joml.Vector2f;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vesko on 01.07.19.
 */
public final class EngineObject implements IStandardObject {

    private Transform transform;

    private List<AbstractComponent> components;

    private List<EngineObject> children;

    private EngineObject parent;

    private boolean active;

    public EngineObject() {
        transform = new Transform(this);
        components = new ArrayList<>();
        EngineManager.addObject(this);
        active = true;
    }

    @Override
    public void awake() {

    }

    @Override
    public void start() {
        if (components != null) {
            components.forEach(IStandardObject::start);
        }
        if (children != null) {
            children.forEach(IStandardObject::start);
        }
    }

    @Override
    public void update() {
        transform.getPosition().add(transform.getFrameVelocity());
        if (components != null) {
            components.forEach(IStandardObject::update);
        }
        if (children != null) {
            children.forEach(IStandardObject::update);
        }
        transform.setFrameVelocity(new Vector2f());
        if (parent != null){
            transform.setRotation(new Quaternionf(parent.transform.getRotation()));
            transform.setPosition(new Vector2f(parent.transform.getPosition()));
            transform.setScale(new Vector2f(parent.transform.getScale()));

        }
    }

    public Transform getTransform() {
        return transform;
    }

    public AbstractComponent getComponent(Class componentType) {
        for (AbstractComponent component : this.components) {
            if (component.getClass().equals(componentType)) {
                return component;
            }
        }
        return null;
    }

    public AbstractComponent addComponent(AbstractComponent component) {
        component.setParentObject(this);
        this.components.add(component);
        return component;
    }

    public void addChild(EngineObject engineObject) {
        if (children == null) {
            children = new ArrayList<>();
        }
        EngineManager.removeObject(engineObject);
        engineObject.parent = this;
        children.add(engineObject);
    }

    public EngineObject getChild(int id) {
        if (id > children.size()) {
            return null;
        } else return children.get(id);
    }

    public EngineObject getParent() {
        return parent;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        if (components != null) {
            components.forEach(abstractComponent -> {
                abstractComponent.setActive(active);
            });
        }
        if (children != null) {
            children.forEach(engineObject -> {
                engineObject.setActive(active);
            });
        }
        this.active = active;
    }
}
