package com.tagelectron.better2dengine;

/**
 * Created by vesko on 01.07.19.
 */
public abstract class AbstractComponent implements IStandardObject{

    protected boolean active;

    protected EngineObject parentObject;

    public void setParentObject(EngineObject engineObject) {
        this.parentObject = engineObject;
    }

    public EngineObject getParentObject() {
        return this.parentObject;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public abstract void destroy();
}
