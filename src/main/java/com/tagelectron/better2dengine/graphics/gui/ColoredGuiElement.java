package com.tagelectron.better2dengine.graphics.gui;

import com.tagelectron.engine.graphics.Model;
import com.tagelectron.engine.rendering.Shader;
import com.tagelectron.engine.text.GLText;
import org.joml.Vector2f;
import org.joml.Vector4f;

public class ColoredGuiElement extends GuiElement {

    private Vector4f color;

    public ColoredGuiElement(Vector2f position, Vector2f scale, Vector4f color, boolean visible) {
        super(position, scale, visible);
        this.color = color;
    }

    public ColoredGuiElement(Vector2f position, Vector2f scale, GLText text, Vector4f color, boolean visible) {
        super(position, scale, text, visible);
        this.color = color;
    }

    public Vector4f getColor() {
        return color;
    }

    public void setColor(Vector4f color) {
        this.color = color;
    }

    @Override
    public void render(Shader textureShader, Shader noTextureShader, Model model) {
        noTextureShader.bind();
        noTextureShader.setUniform("color", color);
        noTextureShader.setUniform("model", modelMatrix);
        noTextureShader.setUniform("projection", GuiManager.getProjection());
        model.render();
        if (text != null) {
            text.render();
        }
    }
}
