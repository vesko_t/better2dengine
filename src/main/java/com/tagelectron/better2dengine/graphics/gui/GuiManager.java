package com.tagelectron.better2dengine.graphics.gui;

import com.tagelectron.better2dengine.io.Camera;
import org.joml.Matrix4f;

import java.util.ArrayList;
import java.util.List;

public class GuiManager {

    private static List<GuiElement> guiElements = new ArrayList<>();

    private static List<GuiElement> entityGuis = new ArrayList<>();

    private static Shader textureShader;

    private static Shader noTextureShader;

    private static final Model model = new Model(new float[]{
            1, 1,
            0, 1,
            0, 0,
            1, 0
    });

    private static Matrix4f projection = new Matrix4f().ortho2D(0, Camera.getWidth(), Camera.getHeight(), 0);

    private static Background background;

    public GuiManager(Shader textureShader, Shader noTextureShader) {
        GuiManager.textureShader = textureShader;
        GuiManager.noTextureShader = noTextureShader;
    }

    public static void render() {
        if (background != null) {
            background.render();
        }
        entityGuis.stream().filter(GuiElement::isVisible).forEach(GuiManager::renderGui);
        guiElements.stream().filter(GuiElement::isVisible).forEach(GuiManager::renderGui);
    }

    private static void renderGui(GuiElement guiElement) {
        guiElement.render(textureShader, noTextureShader, model);
    }

    public static void update() {
        for (int i = 0; i < entityGuis.size(); i++) {
            GuiElement guiElement = entityGuis.get(i);
            if (guiElement.isVisible())
                guiElement.update(0);
        }
        for (int i = 0; i < guiElements.size(); i++) {
            GuiElement guiElement = guiElements.get(i);
            if (guiElement.isVisible())
                guiElement.update(0);
        }
        entityGuis.forEach(guiElement -> guiElement.constantUpdate(0));
        guiElements.forEach(guiElement -> guiElement.constantUpdate(0));
    }

    public static List<GuiElement> getEntityGuis() {
        return entityGuis;
    }

    public static void setEntityGuis(List<GuiElement> entityGuis) {
        GuiManager.entityGuis = entityGuis;
    }

    public static List<GuiElement> getGuiElements() {
        return guiElements;
    }

    public static void setGuiElements(List<GuiElement> guiElements) {
        GuiManager.guiElements = guiElements;
    }

    public static void addGuiElement(GuiElement guiElement){
        guiElements.add(guiElement);
    }

    public static Matrix4f getProjection() {
        return projection;
    }

    public static void setBackground(Background background) {
        GuiManager.background = background;
    }
}
