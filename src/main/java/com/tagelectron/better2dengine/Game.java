package com.tagelectron.better2dengine;

import com.tagelectron.better2dengine.collision.CollisionManager;
import com.tagelectron.better2dengine.graphics.rendering.GraphicsRenderer;
import com.tagelectron.better2dengine.graphics.utils.GraphicsUtils;
import com.tagelectron.better2dengine.io.Camera;
import com.tagelectron.better2dengine.io.Time;
import com.tagelectron.better2dengine.io.Window;

public abstract class Game {

    private static Window window;

    public static void init(Window window) {
        Game.window = window;
        Camera camera = new Camera(Window.getWidth(), Window.getHeight());
        EngineManager.init();
        GraphicsRenderer.init();
    }

    public static void startGame() {
        Time.deltaTime = 0;
        EngineManager.start();

        long lastTime;

        float timePassed = 0;
        int frames = 0;
        while (!window.shouldClose()) {
            lastTime = System.nanoTime();
            window.clear();
            EngineManager.update();
            CollisionManager.update();
            GraphicsRenderer.render();
            window.swapBuffers();
            window.prepare();
            Time.deltaTime = (System.nanoTime() - lastTime) / 1000000000f;
            timePassed += Time.deltaTime;
            frames++;
            if (timePassed >= 1) {
                System.out.println("FPS: " + frames);
                timePassed = 0;
                frames = 0;
            }
        }
        GraphicsUtils.cleanUp();
    }

}
