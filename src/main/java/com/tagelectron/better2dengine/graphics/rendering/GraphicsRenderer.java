package com.tagelectron.better2dengine.graphics.rendering;

import com.tagelectron.better2dengine.IStandardObject;
import com.tagelectron.better2dengine.graphics.shaders.ShaderUtils;
import com.tagelectron.better2dengine.graphics.shaders.StandardShader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vesko on 01.07.19.
 */
public final class GraphicsRenderer {

    private static List<Renderer> renderers;

    private static StandardShader standardShader;

    private GraphicsRenderer() {
    }

    public static void init() {
        standardShader = new StandardShader();
        standardShader.bindAttributeLocation(0, "vertices");
        standardShader.bindAttributeLocation(1, "textures");
    }

    public static void addRenderer(Renderer renderer) {
        if (renderers == null) {
            renderers = new ArrayList<>();
        }
        renderers.add(renderer);
    }

    public static void render() {
        if (renderers != null) {
            renderers.forEach(Renderer::render);
        }
    }

    public static StandardShader getStandardShader() {
        return standardShader;
    }
}
