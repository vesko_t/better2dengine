package com.tagelectron.better2dengine.graphics.rendering;

import com.tagelectron.better2dengine.graphics.Texture;
import org.joml.Vector2f;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vesko on 01.07.19.
 */
public final class SpriteCreator {

    private SpriteCreator() {
    }

    public static Sprite createSprite(Texture texture) {
        Sprite sprite = new Sprite();
        sprite.setTexture(texture);
        sprite.setSquareModel(new SquareModel());
        return sprite;
    }

    public static List<Sprite> sliceSpriteSheet(Texture texture, Vector2f spriteSize) {
        float glWidth = spriteSize.x / texture.getWidth();
        float glHeight = spriteSize.y / texture.getHeight();

        int columnCount = (int) (texture.getWidth() / spriteSize.x);
        int rowCount = (int) (texture.getHeight() / spriteSize.y);

        List<Sprite> sprites = new ArrayList<>();

        for (int i = 0; i < rowCount; i++) {
            for (int j = 0; j < columnCount; j++) {
                Sprite sprite = new Sprite();
                float[] textureCoordinates = new float[]{
                        j * glWidth, i * glHeight,
                        j * glWidth + glWidth, i * glHeight,
                        j * glWidth + glWidth, i * glHeight + glHeight,
                        j * glWidth, i * glHeight + glHeight
                };
                sprite.setTexture(texture);
                sprite.setSquareModel(new SquareModel(textureCoordinates));
                sprites.add(sprite);
            }
        }

        return sprites;
    }

}
