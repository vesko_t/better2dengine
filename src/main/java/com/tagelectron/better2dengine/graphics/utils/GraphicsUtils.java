package com.tagelectron.better2dengine.graphics.utils;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL15;
import org.lwjgl.system.MemoryStack;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL15.GL_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.GL_STATIC_DRAW;
import static org.lwjgl.system.MemoryStack.stackPush;

/**
 * Created by vesko on 01.07.19.
 */
public final class GraphicsUtils {

    private static List<Integer> vbos = new ArrayList<>();

    private GraphicsUtils() {
    }

    public static int createVbo(float[] data) {
        int bufferId = glGenBuffers();
        glBindBuffer(GL_ARRAY_BUFFER, bufferId);
        FloatBuffer floatBuffer = BufferUtils.createFloatBuffer(data.length);
        floatBuffer.put(data);
        floatBuffer.position(0);
        glBufferData(GL_ARRAY_BUFFER, floatBuffer, GL_STATIC_DRAW);
        vbos.add(bufferId);
        return bufferId;
    }

    public static int createVbo(int[] data) {
        int bufferId = glGenBuffers();
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferId);
        IntBuffer intBuffer = BufferUtils.createIntBuffer(data.length);
        intBuffer.put(data);
        intBuffer.position(0);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, intBuffer, GL_STATIC_DRAW);
        vbos.add(bufferId);
        return bufferId;
    }

    public static void cleanUp() {
        vbos.forEach(GL15::glDeleteBuffers);
    }

}
